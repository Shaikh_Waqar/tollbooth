/**
 * Created by waqarahmeds on 7/27/2017.
 */

import ITollModel = require("./interfaces/TollModel");

class TollModel {

    private _tollModel: ITollModel;

    constructor(){
    }
    /*constructor(tollModel: ITollModel) {
        this._tollModel = tollModel;
    }*/
    get redeemed (): boolean {
        return this._tollModel.redeemed;
    }

    get code (): number {
        return this._tollModel.code;
    }

    get vehicleType (): string {
        return this._tollModel.vehicleType;
    }

    get price (): number {
        return this._tollModel.price;
    }

    get towards (): string {
        return this._tollModel.towards;
    }

    set redeemed (redeemed: boolean) {
        this._tollModel.redeemed = redeemed;
    }

    set code (code :number) {
        this._tollModel.code = code;
    }

    set vehicleType (vehicleType: string) {
        this._tollModel.vehicleType = vehicleType;
    }

    set price (price : number) {
        this._tollModel.price = price;
    }

    set towards (towards: string) {
        this._tollModel.towards = towards;
    }
}
export =  TollModel;