import TollCatPrice = require("./TollCatPrice");
import {Map} from "gulp-typescript/release/utils";
/**
 * Created by waqarahmeds on 7/31/2017.
 */

class ExitDestinationModel{
    exitLocation: Map<TollCatPrice>;
}
export = ExitDestinationModel;