/**
 * Created by waqarahmeds on 7/27/2017.
 */

import Tool = require("./../TollModel");
import mongoose = require("mongoose");

interface UserModel extends mongoose.Document {
    name: string;
    emailId: string;
    password: string;
    tolls: Array<Tool>;
}

export = UserModel;
