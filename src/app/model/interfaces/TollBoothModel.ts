/**
 * Created by waqarahmeds on 7/27/2017.
 */


import mongoose = require("mongoose");
import Tool = require("./../TollModel");
import ExitDestinationModel = require("../ExitDestinationModel");
import {Map} from "gulp-typescript/release/utils";

interface TollBoothModel extends mongoose.Document {
    name: string;
    state: string;
    emailId: string;
    password: string;
    /*tolls: [{type:mongoose.Schema.Types.ObjectId, ref:'Toll'}];*/
    tolls: Array<Tool>;
    tollCategory:Map<ExitDestinationModel>;
    geoLocation:Array<number>;
    towards : Array<string>;
    tollBoothBeacon: string;

}

export = TollBoothModel;
