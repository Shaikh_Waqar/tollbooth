/**
 * Created by waqarahmeds on 7/27/2017.
 */


import mongoose = require("mongoose");

interface TollModel extends mongoose.Document {
    redeemed: boolean;
    code: number;
    vehicleType: string;
    price: number;
    towards: string;
    exit: string;
}

export = TollModel;
