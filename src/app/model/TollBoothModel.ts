/**
 * Created by waqarahmeds on 7/27/2017.
 */

import ITollBoothModel = require("./interfaces/TollBoothModel");

class TollBoothModel {

    private _tollBoothModel: ITollBoothModel;

    constructor(tollBooth: ITollBoothModel) {
        this._tollBoothModel = tollBooth;
    }

}
Object.seal(TollBoothModel);
export =  TollBoothModel;