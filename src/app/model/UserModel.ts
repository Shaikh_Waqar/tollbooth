/**
 * Created by waqarahmeds on 7/27/2017.
 */

import IUserModel = require("./interfaces/UserModel");

class UserModel {

    private _userModel: IUserModel;

    constructor(userModel: IUserModel) {
        this._userModel = userModel;
    }
    get name (): string {
        return this._userModel.name;
    }

    get emailId (): string {
        return this._userModel.emailId;
    }

    get password (): string {
        return this._userModel.password;
    }


}
Object.seal(UserModel);
export =  UserModel;