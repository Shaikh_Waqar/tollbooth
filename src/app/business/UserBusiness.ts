/**
 * Created by waqarahmeds on 7/27/2017.
 */
import UserRepository = require("./../repository/UserRepository");
import IUserBusiness = require("./interfaces/UserBusiness");
import IUserModel = require("./../model/interfaces/UserModel");
import ITollModel = require("./../model/interfaces/TollModel");
import TollBusiness = require("./TollBusiness");
import TollModel = require("../model/TollModel");
import TollBoothRepository = require("../repository/TollBoothRepository");

var log4js = require('log4js');
var logger = log4js.getLogger('UserBusiness');



class UserBusiness  implements IUserBusiness {
    private _userRepository: UserRepository;
    private _tollBusiness: TollBusiness;
    private _tollBoothRepository: TollBoothRepository;

    constructor () {
        this._userRepository = new UserRepository();
        this._tollBusiness = new TollBusiness();
        this._tollBoothRepository = new TollBoothRepository();
    }

    create (item: IUserModel, callback: (error: any, result: any) => void) {
        this._userRepository.create(item, callback);
    }

    retrieve (fields, projection: string, callback: (error: any, result: any) => void) {
        this._userRepository.retrieve(fields, projection, callback);
    }

    update (_id: string, item: IUserModel, callback: (error: any, result: any) => void) {
        this._userRepository.findById(_id, null, (err, res) => {
            if(err) callback(err, res);
            else
                this._userRepository.update(res._id, item, callback);

        });
    }

    delete (_id: string, callback:(error: any, result: any) => void) {
        this._userRepository.delete(_id, callback);
    }

    findById (_id: string, projection: string, callback: (error: any, result: IUserModel) => void) {
        this._userRepository.findById(_id, projection, callback);
    }

    login(userModel: IUserModel, callback:(error: any, result: any) =>void){

        this.retrieve(userModel, "name emailId", (error: any, result: any)=>{
            if(error){
                callback(error, null);
            } else{
                if(result.length > 0)
                {
                    callback(null, result[0]);
                }else{
                    callback(new Error("UserName of password not found"), null);
                }
            }
        })
    }

    purchaseToll(data, callback:(error: any, result: any)=> void){
        let tollModel :ITollModel = <ITollModel>data.tollCategory;
        tollModel.code = Math.floor(Math.random()*89999+100000);
        this._tollBusiness.create(tollModel, (error :any, toll: any)=> {
            if(error){
                logger.error(error);
                callback(error, null);
            }else{
                this._userRepository.findOneAndUpdate({"_id":data.userId},{$push:{"tolls":toll._id}},{new : true}, (error, status)=>{
                    if(error){
                        logger.error(error);
                        callback(error, null);
                    }
                    else{
                        this._tollBoothRepository.findOneAndUpdate({"_id":data.tollBoothId},{$push:{"tolls":toll._id}},{new : true},
                            (error, status)=>{
                            if(error){
                                logger.error(error);
                                callback(error, null);
                            }
                            else{
                                callback(null, "Done");
                            }
                        })
                    }
                })
            }
        })
    }

    getAllToll(query, callback:(error: any, result: any)=> void){
        let populateField = "tolls";
        console.log("query => "+ query);
        this._userRepository.findAndPopulate(query, "tolls", populateField, callback);
    }

    travelerArrived(data, callback:(error: any, result: any)=> void){
        var query = {"tollBoothBeacon":data.tollBoothBeacon};
        var newData = {$push:{"arrivedTreveler":data.toll._id}};
        var optionAndProjection = {
                                    new : true, select: {
                                        arrivedTreveler: {
                                            $elemMatch:{$in: [data.toll._id]}
                                             }
                                        }
                                    }
        this._tollBoothRepository.findOneAndUpdate(query,
            newData, optionAndProjection, callback);
    }
}
Object.seal(UserBusiness);
export = UserBusiness;