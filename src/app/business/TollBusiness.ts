/**
 * Created by waqarahmeds on 7/27/2017.
 */
import TollRepository = require("./../repository/TollRepository");
import ITollBusiness = require("./interfaces/TollBusiness");
import ITollModel = require("./../model/interfaces/TollModel");

class TollBusiness  implements ITollBusiness {
    private _tollRepository: TollRepository;

    constructor () {
        this._tollRepository = new TollRepository();
    }

    create (item: ITollModel, callback: (error: any, result: any) => void) {
        this._tollRepository.create(item, callback);
    }

    retrieve (fields, projection: string, callback: (error: any, result: any) => void) {
        this._tollRepository.retrieve(fields, projection, callback);
    }

    update (_id: string,  item: ITollModel, callback: (error: any, result: any) => void) {
        this._tollRepository.findById(_id, null, (err, res) => {
            if(err) callback(err, res);
            else
                this._tollRepository.update(res._id, item, callback);

        });
    }

    delete (_id: string, callback:(error: any, result: any) => void) {
        this._tollRepository.delete(_id, callback);
    }

    findById (_id: string, projection: string, callback: (error: any, result: ITollModel) => void) {
        this._tollRepository.findById(_id, projection, callback);
    }

}
Object.seal(TollBusiness);
export = TollBusiness;