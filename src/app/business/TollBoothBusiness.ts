/**
 * Created by waqarahmeds on 7/27/2017.
 */
import TollBoothRepository = require("./../repository/TollBoothRepository");
import ITollBoothBusiness = require("./interfaces/TollBoothBusiness");
import ITollBoothModel = require("./../model/interfaces/TollBoothModel");
import TollRepository = require("../repository/TollRepository");

var log4js = require('log4js');
var logger = log4js.getLogger('TollBoothBusines');

class TollBoothBusiness  implements ITollBoothBusiness {
    private _tollBoothRepository: TollBoothRepository;
    private _tollRepository: TollRepository;

    constructor () {
        this._tollBoothRepository = new TollBoothRepository();
        this._tollRepository = new TollRepository();
    }

    create (item: ITollBoothModel, callback: (error: any, result: any) => void) {
        this._tollBoothRepository.create(item, callback);
    }

    retrieve (fields, projection, callback: (error: any, result: any) => void) {
        this._tollBoothRepository.retrieve(fields, projection, callback);
    }

    update (_id: string, item: ITollBoothModel, callback: (error: any, result: any) => void) {
        this._tollBoothRepository.findById(_id, null,(err, res) => {
            if(err) callback(err, res);
            else
                this._tollBoothRepository.update(res._id, item, callback);

        });
    }

    delete (_id: string, callback:(error: any, result: any) => void) {
        this._tollBoothRepository.delete(_id, callback);
    }

    findById (_id: string, projection: string, callback: (error: any, result: ITollBoothModel) => void) {
        this._tollBoothRepository.findById(_id, projection, callback);
    }

    login(tollBoothModel: ITollBoothModel, callback:(error: any, result: any) =>void){

        this.retrieve(tollBoothModel, "name emailId", (error: any, result: any)=>{
           if(error){
              callback(error, null);
           } else{
               if(result.length > 0)
               {
                    callback(null, result[0]);
               }else{
                   callback(new Error("UserName of password not found"), null);
               }
           }
        })
    }

    getAllTollBooth(tollBoothModel: ITollBoothModel, callback:(error: any, result: any) => void){
        let query = JSON.parse(JSON.stringify(tollBoothModel));
        if(tollBoothModel.geoLocation){
            let maxDistance = 8;
            maxDistance /= 6371;
            query.geoLocation = {
                $near: tollBoothModel.geoLocation,
                $maxDistance: maxDistance
            }
        }
        this.retrieve(query, "name state", callback);
    }

    getTollCategory(_id, callback:(error: any, result: any) =>void ){
        let projection = "tollCategory towards";
        this.findById(_id, projection, callback);
    }

    getAllToll(query, callback:(error: any, result: any)=> void){
        let populateField = {
            "path":"tolls",
            "match":{"redeemed": false}
        }
        console.log("query => "+ JSON.stringify(query));
        this._tollBoothRepository.findAndPopulate(query, "tolls",populateField, callback);
    }

    redeemToll(query, callback:(error: any, result:any )=> void){
        let newData = { "redeemed": true};
        this._tollRepository.findOneAndUpdate(query, newData, {new: true}, callback);
    }
    getArrivedTraveler(query, callback:(error: any, result: any)=> void){
        let populateField = {
            "path":"arrivedTreveler",
            "match":{"redeemed": false}
        }
        console.log("query => "+ JSON.stringify(query));
        var projection = "arrivedTreveler";
        this._tollBoothRepository.findAndPopulate(query, projection, populateField, callback);
    }

    approveToll(data, callback:(error: any, result: any)=> void){
        let tollUpdateQuery = { _id: data.toll._id};
        let tollUpdateData = { redeemed: true};
        let tollBoothUpateQuery = { _id: data.tollBoothId};
        let tollBoothUpdateData = {$pull:{"arrivedTreveler": data.toll._id}};
        this._tollRepository.findOneAndUpdate(tollUpdateQuery,tollUpdateData,{new: true},
            (error: any,result: any) =>{
            if(error){
                callback(new Error("unable to update toll detail =>" + error.message),null);
            }else{
                this._tollBoothRepository.findOneAndUpdate(tollBoothUpateQuery, tollBoothUpdateData,{},
                    (error: any, result: any) =>{
                        if(error){
                            callback(new Error("unable to update tollbooth detail =>" + error.message),null);
                        } else{
                          callback(null, "done");
                        }
                    });
            }
            });
    }
}
Object.seal(TollBoothBusiness);
export = TollBoothBusiness;