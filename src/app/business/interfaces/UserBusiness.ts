/**
 * Created by waqarahmeds on 7/27/2017.
 */

import BaseBusiness = require("./base/BaseBusiness");
import IUserModel = require("./../../model/interfaces/UserModel");

interface UserBusiness extends BaseBusiness<IUserModel> {

}
export = UserBusiness;
