/**
 * Created by waqarahmeds on 7/27/2017.
 */

import BaseBusiness = require("./base/BaseBusiness");
import ITollModel = require("./../../model/interfaces/TollModel");

interface TollBusiness extends BaseBusiness<ITollModel> {

}
export = TollBusiness;
