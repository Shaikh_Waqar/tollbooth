
interface Read<T> {
    retrieve: (fields, projection: string, callback: (error: any, result: T)=> void)=> void ;
    findById: (_id: string, projection: string, callback: (error:any, result: T) => void) => void;
       
}

export = Read;