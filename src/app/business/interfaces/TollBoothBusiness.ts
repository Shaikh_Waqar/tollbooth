/**
 * Created by waqarahmeds on 7/27/2017.
 */

import BaseBusiness = require("./base/BaseBusiness");
import ITollBoothModel = require("./../../model/interfaces/TollBoothModel");

interface TollBoothBusiness extends BaseBusiness<ITollBoothModel> {

}
export = TollBoothBusiness;
