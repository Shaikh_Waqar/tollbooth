/**
 * Created by waqarahmeds on 7/27/2017.
 */

import DataAccess = require("./../../dataAccess/DataAccess");
import ITollModel = require("./../../model/interfaces/TollModel");

var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;

class TollSchema {

    static get schema() {
        var schema = new mongoose.Schema({
            redeemed: {
                type: Boolean,
                default: false
            },
            code: {
                type: Number,
                required: true
            },
            vehicleType: {
                type: String,
                required: true
            },
            price: {
                type: Number,
                required: true
            },
            towards: {
                type: String,
                required: true
            },
            exit: {
                type: String,
                required: true
            }
        }, { timestamps: {} });

        return schema;
    }
}
var schema = mongooseConnection.model<ITollModel>("Toll", TollSchema.schema);
export = schema;

