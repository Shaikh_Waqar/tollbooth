/**
 * Created by waqarahmeds on 7/27/2017.
 */

import DataAccess = require("./../../dataAccess/DataAccess");
import IUserModel = require("./../../model/interfaces/UserModel");

var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;

class UserSchema {

    static get schema() {
        var schema = new mongoose.Schema({
            name: {
                type: String,
                required: true
            },
            emailId: {
                type: String,
                required: true,
                unique: true
            },
            password: {
                type: String,
                required: true
            },
            tolls:[{
                type:mongoose.Schema.Types.ObjectId, ref:'Toll'
            }]
        }, { timestamps: {} });

        return schema;
    }
}
var schema = mongooseConnection.model<IUserModel>("User", UserSchema.schema);
export = schema;

