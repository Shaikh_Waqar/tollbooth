/**
 * Created by waqarahmeds on 7/27/2017.
 */

import DataAccess = require("./../../dataAccess/DataAccess");
import ITollBoothModel = require("./../../model/interfaces/TollBoothModel");
import IToll = require("./../../model/interfaces/TollModel");

var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;

class TollBoothSchema {

    static get schema() {
        var schema = new mongoose.Schema({
            name: {
                type: String,
                required: true
            },
            state:{
                type: String,
                required: true
            },
            emailId: {
                type: String,
                required: true,
                unique: true
            },
            password: {
                type: String,
                required: true
            },
            towards:[String],
            tolls:[{
                type:mongoose.Schema.Types.ObjectId, ref:'Toll'
            }],
            arrivedTreveler:[{
                type:mongoose.Schema.Types.ObjectId, ref:'Toll'
            }],
            tollCategory:{
                type: Object,
            },
            geoLocation: {
                type: [Number],
                index: '2d'
            },
            tollBoothBeacon: {
                type: String,
                required: true
            }
        }, { timestamps: {} });

        return schema;
    }
}
var schema = mongooseConnection.model<ITollBoothModel>("TollBooth", TollBoothSchema.schema);
export = schema;

