/**
 * Created by waqarahmeds on 7/27/2017.
 */
import TollBoothModel = require("./../model/TollBoothModel");
import ITollBoothModel = require("./../model/interfaces/TollBoothModel");
import TollBoothSchema = require("./../dataAccess/schemas/TollBoothSchema");
import RepositoryBase = require("./base/RepositoryBase");

class TollBoothRepository  extends RepositoryBase<ITollBoothModel> {
    constructor () {
        super(TollBoothSchema);
    }

}

Object.seal(TollBoothRepository);
export = TollBoothRepository;
