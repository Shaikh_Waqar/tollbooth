/**
 * Created by waqarahmeds on 7/27/2017.
 */
import TollModel = require("./../model/TollModel");
import ITollModel = require("./../model/interfaces/TollModel");
import TollSchema = require("./../dataAccess/schemas/TollSchema");
import RepositoryBase = require("./base/RepositoryBase");

class TollRepository  extends RepositoryBase<ITollModel> {
    constructor () {
        super(TollSchema);
    }

}

Object.seal(TollRepository);
export = TollRepository;
