interface Read<T> {
    retrieve: (fields, projection: string, callback: (error: any, result: any)=> void)=> void;
    findById: (id: string, projection: string, callback: (error:any, result: T) => void) => void;
} 

export = Read;
