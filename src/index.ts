
    import express = require("express");
    import config = require("config");
    import log4js = require("log4js");
    import Middlewares = require("./middlewares/base/MiddlewaresBase");
    import {IConfig} from "log4js";

    var loggerConfig : IConfig = JSON.parse(JSON.stringify(config.get("logger"))); ;
    log4js.configure(loggerConfig);
    var logger = log4js.getLogger('index');

    if (!process.env.NODE_ENV) {
        logger.info("Bridge ENV not found setting default.");
        process.env.NODE_ENV = 'development';
    }

    var app = express();
    var port = parseInt(process.env.PORT, 10) || config.get("APP_PORT");
    app.set("port", port);
    app.use(Middlewares.configuration);
    
    app.listen(port, () => {
        logger.info("Node app is running at localhost:" + port);
       
    });
