/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import TollBusiness= require("./../app/business/TollBusiness");
import IBaseController = require("./interfaces/base/BaseController");
import ITollModel = require("./../app/model/interfaces/TollModel");



class TollController implements IBaseController <TollBusiness> {

    create(req: express.Request, res: express.Response): void {
        try {

            var toll: ITollModel = <ITollModel>req.body;
            var tollBusiness = new TollBusiness();
            tollBusiness.create(toll, (error, result) => {
                if(error) res.send({"error": error});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
    update(req: express.Request, res: express.Response): void {
        try {
            var toll: ITollModel = <ITollModel>req.body;
            var _id: string = req.params._id;
            var tollBusiness = new TollBusiness();
            tollBusiness.update(_id, toll, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    delete(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;
            var tollBusiness = new TollBusiness();
            tollBusiness.delete(_id, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    retrieve(req: express.Request, res: express.Response): void {
        try {

            var tollBusiness = new TollBusiness();
            tollBusiness.retrieve({}, {}, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    findById(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;

            var tollBusiness = new TollBusiness();
            tollBusiness.findById(_id, null, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }

}
export = TollController;