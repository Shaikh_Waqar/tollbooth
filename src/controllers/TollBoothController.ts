/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import TollBoothBusiness= require("./../app/business/TollBoothBusiness");
import IBaseController = require("./interfaces/base/BaseController");
import ITollBoothModel = require("./../app/model/interfaces/TollBoothModel");

var log4js = require('log4js');
var logger = log4js.getLogger('TollBoothController');

class TollBoothController implements IBaseController <TollBoothBusiness> {

    create(req: express.Request, res: express.Response): void {
        try {
            logger.info("create TollBooth is been hit.");
            var tollBooth: ITollBoothModel = <ITollBoothModel>req.body;
            logger.info(" tollBooth model => "+ JSON.stringify(tollBooth));
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.create(tollBooth, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
    update(req: express.Request, res: express.Response): void {
        try {
            var tollBooth: ITollBoothModel = <ITollBoothModel>req.body;
            var _id: string = req.params._id;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.update(_id, tollBooth, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    delete(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.delete(_id, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    retrieve(req: express.Request, res: express.Response): void {
        try {

            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.retrieve({}, {}, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    findById(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;

            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.findById(_id, null, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    login(req: express.Request, res: express.Response): void {
        try {

            var tollBooth: ITollBoothModel = <ITollBoothModel>req.body;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.login(tollBooth, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
    getAllTollBooth(req: express.Request, res: express.Response): void {
        try {

            var tollBooth: ITollBoothModel = <ITollBoothModel>req.body;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.getAllTollBooth(tollBooth, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
    getTollCategory(req: express.Request, res: express.Response): void {
        try {

            let _id = req.params._id;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.getTollCategory(_id, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    getAllToll(req: express.Request, res: express.Response): void {
        try {
            let data = {"_id" : req.params._id};
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.getAllToll(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    redeemToll(req: express.Request, res: express.Response): void {
        try {
            let data = {"_id" : req.params._id};
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.redeemToll(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    getArrivedTraveler(req: express.Request, res: express.Response): void {
        try {
            let data = {"_id" : req.params._id};
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.getArrivedTraveler(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    approveToll(req: express.Request, res: express.Response): void {
        try {
            let data = req.body;
            data["tollBoothId"] = req.params._id;
            var tollBoothBusiness = new TollBoothBusiness();
            tollBoothBusiness.approveToll(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
}
export = TollBoothController;