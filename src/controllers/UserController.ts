/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import UserBusiness= require("./../app/business/UserBusiness");
import IBaseController = require("./interfaces/base/BaseController");
import IUserModel = require("./../app/model/interfaces/UserModel");

var log4js = require('log4js');
var logger = log4js.getLogger("UserController");

class UserController implements IBaseController <UserBusiness> {

    create(req: express.Request, res: express.Response): void {
        try {

            var user: IUserModel = <IUserModel>req.body;
            var userBusiness = new UserBusiness();
            userBusiness.create(user, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }
    update(req: express.Request, res: express.Response): void {
        try {
            var user: IUserModel = <IUserModel>req.body;
            var _id: string = req.params._id;
            var userBusiness = new UserBusiness();
            userBusiness.update(_id, user, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    delete(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;
            var userBusiness = new UserBusiness();
            userBusiness.delete(_id, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"success": "success"});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    retrieve(req: express.Request, res: express.Response): void {
        try {

            var userBusiness = new UserBusiness();
            userBusiness.retrieve({}, null, (error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    findById(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;

            var userBusiness = new UserBusiness();
            userBusiness.findById(_id, null,(error, result) => {
                if(error) res.send({"error": "error"});
                else res.send(result);
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});

        }
    }
    login(req: express.Request, res: express.Response): void {
        try {

            var user: IUserModel = <IUserModel>req.body;
            var userBusiness = new UserBusiness();
            userBusiness.login(user, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    purchaseToll(req: express.Request, res: express.Response): void {
        try {

            var data = req.body;
            var userBusiness = new UserBusiness();
            userBusiness.purchaseToll(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    getAllToll(req: express.Request, res: express.Response): void {
        try {
            let data = {"_id" : req.params._id};
            var userBusiness = new UserBusiness();
            userBusiness.getAllToll(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

    travelerArrived(req: express.Request, res: express.Response): void {
        try {

            var data = req.body;
            data["tollBoothBeacon"] = req.params._id;
            var userBusiness = new UserBusiness();
            userBusiness.travelerArrived(data, (error, result) => {
                if(error) res.send({"error": error.message});
                else res.send({"result": result});
            });
        }
        catch (e)  {
            console.log(e);
            res.send({"error": "error in your request"});
        }
    }

}
export = UserController;