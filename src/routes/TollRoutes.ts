/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import TollController = require("./../controllers/TollController");

var router = express.Router();
class TollRoutes {
    private _tollController: TollController;

    constructor () {
        this._tollController = new TollController();
    }
    get routes () : express.Router {

        var controller = this._tollController;
        router.get("/toll", controller.retrieve);
        router.post("/toll", controller.create);
        router.put("/toll/:_id", controller.update);
        router.get("/toll/:_id", controller.findById);
        router.delete("/toll/:_id", controller.delete);

        return router;
    }


}

Object.seal(TollRoutes);
export = TollRoutes;
