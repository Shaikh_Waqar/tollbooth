/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import TollBoothController = require("./../controllers/TollBoothController");

var router = express.Router();
class TollBoothRoutes {
    private _tollBoothController: TollBoothController;

    constructor () {
        this._tollBoothController = new TollBoothController();
    }
    get routes () : express.Router {

        var controller = this._tollBoothController;
        router.get("/tollbooth", controller.retrieve);
        router.post("/tollbooth", controller.create);
        router.put("/tollbooth/:_id", controller.update);
        router.get("/tollbooth/:_id", controller.findById);
        router.delete("/tollbooth/:_id", controller.delete);
        router.post("/tollbooth/login", controller.login);
        router.post("/tollbooth/all", controller.getAllTollBooth);
        router.get("/tollbooth/toll/category/:_id", controller.getTollCategory);
        router.get("/tollbooth/tolls/:_id", controller.getAllToll);
        router.get("/tollbooth/redeem/toll/:_id", controller.redeemToll);
        router.get("/tollbooth/arrived/traveler/:_id", controller.getArrivedTraveler);
        router.post("/tollbooth/approve/toll/:_id", controller.approveToll);


        return router;
    }


}

Object.seal(TollBoothRoutes);
export = TollBoothRoutes;
