import express = require("express");
import UserRoutes = require("./../UserRoutes");
import TollBoothRoutes = require("./../TollBoothRoutes");
import TollRoutes = require("./../TollRoutes");

var app = express();
class BaseRoutes {
    
    get routes() {
        app.use("/", new UserRoutes().routes);
        app.use("/", new TollBoothRoutes().routes);
        app.use("/", new TollRoutes().routes);
        return app;
    }
}
export = BaseRoutes;