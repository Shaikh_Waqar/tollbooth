/**
 * Created by waqarahmeds on 7/27/2017.
 */
import express = require("express");
import UserController = require("./../controllers/UserController");

var router = express.Router();
class UserRoutes {
    private _userController: UserController;

    constructor () {
        this._userController = new UserController();
    }
    get routes () : express.Router {

        var controller = this._userController;
        router.get("/user", controller.retrieve);
        router.post("/user", controller.create);
        router.put("/user/:_id", controller.update);
        router.get("/user/:_id", controller.findById);
        router.delete("/user/:_id", controller.delete);
        router.post("/user/login", controller.login);
        router.post("/user/purchase/toll", controller.purchaseToll);
        router.get("/user/tolls/:_id", controller.getAllToll);
        router.post("/user/tolls/arrived/tollbooth/:_id", controller.travelerArrived);

        return router;
    }


}

Object.seal(UserRoutes);
export = UserRoutes;
