var gulp = require("gulp");
var clean = require('gulp-clean');
var tsc = require('gulp-typescript');
var nodemon = require("gulp-nodemon");
var minify = require('gulp-minify');
var sourcemaps = require('gulp-sourcemaps');

gulp.task("copy", [ "public", "config", "package-json" ]);
gulp.task("default", [ "watch", "nodemon" ]);


gulp.task('clean', function() {
    return gulp
        .src([ 'dist/*' ], { read: false })
        .pipe(clean());
});

gulp.task('public', [ 'clean' ], function() {
    return gulp
        .src('src/public/**')
        .pipe(gulp.dest('./dist/public'));
});

gulp.task('config', [ 'clean' ], function() {
    return gulp
        .src('config/**')
        .pipe(gulp.dest('./dist/config'));
});

gulp.task('package-json', [ 'clean' ], function() {
    return gulp
        .src('package.json')
        .pipe(gulp.dest('./dist'));
});

gulp.task("build", [ 'copy' ], function () {
    return gulp
        .src("src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsc({
            "module": "commonjs",
            "target": "ES5",
            "noImplicitAny": false,
            "emitDecoratorMetadata": true,
            "experimentalDecorators": true/*,
            "logErrors": true*/
        }))
        .pipe(sourcemaps.write('./'))
        /*.pipe(minify({
         ext: {
         src: "-debug.js",
         min: ".js"
         },
         noSource: true,
         exclude: [ "logs", "public" ],
         ignoreFiles: []
         }))*/
        .pipe(gulp.dest('dist'));
});

gulp.task("watch", [ 'build' ], function () {
    return gulp.watch("src/**/*.*", [ "build" ]);
});

gulp.task("nodemon", [ 'build' ], function () {
    nodemon({ script: "dist/index.js" });
});
